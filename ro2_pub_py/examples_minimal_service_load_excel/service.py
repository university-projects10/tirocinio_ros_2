# Copyright 2016 Open Source Robotics Foundation, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import rclpy
import pandas as pd
import os



def main(args=None):
    rclpy.init(args=args)
    node = rclpy.create_node('examples_minimal_service_load_exo_csv')
    cwd = os.getcwd()
    csvFilename=cwd +'\\ro2_pub_py\\examples_minimal_service_load_excel\\sub.csv'
    Data_Past_Bystronic = csvFilename
    save_name_exo = input("insert the name of your exo: ")
    print(save_name_exo)
    exe_time_ad_no_exo, total_err_exo, total_err_no_exo, total_time_exo, total_time_no_exo = without_exo(
        Data_Past_Bystronic, csvFilename)

    #execution time: total time ascending/descending
    exe_time_ad_exo = input("insert the total time for ascending/descendig with exo [s]: ")
    float(exe_time_ad_exo)
    print(exe_time_ad_exo)

    total_err_resp_diff = total_err_exo - total_err_no_exo
    tot_time_diff = total_time_exo - total_time_no_exo
    tot_ad_time_diff = float(exe_time_ad_exo) - float(exe_time_ad_no_exo)

    print(total_err_resp_diff)
    print(tot_time_diff)
    print(tot_ad_time_diff)

    write_file(cwd, exe_time_ad_exo, exe_time_ad_no_exo, save_name_exo, tot_ad_time_diff, tot_time_diff, total_err_exo,
               total_err_no_exo, total_err_resp_diff, total_time_exo, total_time_no_exo)

    rclpy.spin(node)

    # Destroy the timer attached to the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    node.destroy_timer(timer)
    node.destroy_node()
    rclpy.shutdown()


def write_file(cwd, exe_time_ad_exo, exe_time_ad_no_exo, save_name_exo, tot_ad_time_diff, tot_time_diff, total_err_exo,
               total_err_no_exo, total_err_resp_diff, total_time_exo, total_time_no_exo):
    f_metrics_HR_df = pd.DataFrame(
        {'total_errors (no exo, exo, difference)': [total_err_no_exo, total_err_exo, total_err_resp_diff],
         'total_time_resp (no exo, exo, difference)': [total_time_no_exo, total_time_exo, tot_time_diff],
         'total_time_ad (no exo, exo, difference)': [tot_ad_time_diff, exe_time_ad_no_exo, exe_time_ad_exo]})
    save_name = cwd + '\\ro2_pub_py\\resource\\HR_metrics_STEPbySTEP_' + save_name_exo + '.csv'
    print(save_name)
    f_metrics_HR_df.to_csv(save_name, index=False, header=True)


def without_exo(Data_Past_Bystronic, csvFilename):
    df_no_exo = pd.read_csv(Data_Past_Bystronic)
    df_no_exo
    total_resp_df_no_exo = df_no_exo.filter(regex='total_responses')
    total_resp_correct_df_no_exo = df_no_exo.filter(regex='total_correct')
    dim_df_no_exo = total_resp_df_no_exo.size
    total_err_no_exo = total_resp_df_no_exo.iloc[dim_df_no_exo - 1, 0:1].values - total_resp_correct_df_no_exo.iloc[
                                                                                  dim_df_no_exo - 1, 0:1].values
    print(total_resp_df_no_exo.iloc[dim_df_no_exo - 1, 0:1].values)
    print(total_resp_correct_df_no_exo.iloc[dim_df_no_exo - 1, 0:1].values)
    print(total_err_no_exo)
    total_resp_no_exo = df_no_exo.filter(regex='total_responses')
    # total time: sum of times from question to answer
    total_time_df_no_exo = df_no_exo.filter(regex='total_response_time')
    total_time_no_exo = total_time_df_no_exo.iloc[dim_df_no_exo - 1, 0:1].values / 1000  # [s]
    print(total_time_no_exo)
    # execution time: total time ascending/descending
    exe_time_ad_no_exo = input("insert the total time for ascending/descendig without exo [s]: ")
    float(exe_time_ad_no_exo)
    print(exe_time_ad_no_exo)
    Data_Past_Bystronic = csvFilename
    df_exo = pd.read_csv(Data_Past_Bystronic)
    df_exo
    total_resp_df_exo = df_exo.filter(regex='total_responses')
    total_resp_correct_df_exo = df_exo.filter(regex='total_correct')
    dim_df_exo = total_resp_df_exo.size
    total_err_exo = total_resp_df_exo.iloc[dim_df_exo - 1, 0:1].values - total_resp_correct_df_exo.iloc[dim_df_exo - 1,
                                                                         0:1].values
    print(total_resp_df_exo.iloc[dim_df_exo - 1, 0:1].values)
    print(total_resp_correct_df_exo.iloc[dim_df_exo - 1, 0:1].values)
    print(total_err_exo)
    total_resp_exo = df_exo.filter(regex='total_responses')
    # total time: sum of times from question to answer
    total_time_df_exo = df_exo.filter(regex='total_response_time')
    total_time_exo = total_time_df_exo.iloc[dim_df_exo - 1, 0:1].values / 1000  # [s]
    print(total_time_exo)
    return exe_time_ad_no_exo, total_err_exo, total_err_no_exo, total_time_exo, total_time_no_exo


if __name__ == '__main__':
    main()